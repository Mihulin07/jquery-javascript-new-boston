$(document).ready(function(){
	alert('Ready');  // pojavi se ko se stran
});

$('#image1').load(function(){
	alert('Image1 loaded'); // pojavi se ko se naloži image1
});

$('#iframe').load(function(){
	alert('iframe loaded'); // pojavi se ko se naloži iframe
});

$('img').load(function(){
	alert('All images loaded');  // pojavi se ko se naloži vsaka slika
});

$('#iframe3').load(function(){
	alert('iframe3 loaded');  // pojavi se ko se naloži iframe3
});

$('iframe').load(function(){
	alert('All iframe are loaded'); //ko se vsak iframe naloži se pojavi
});

$(window).load(function(){
	alert('Window is loaded');  //ko se vse naloži na strani, se pojavi ta alert
});

$(window).unload(function(){
	var c = confirm ('Ali si siguren?');
	if (c){
		return true;
	}
	else{
		return false;
	}
	
	alert('Window is unloaded');  //ko gre user stran od strani, se pojavi ta alert //dela samo v starejših browserjih
});