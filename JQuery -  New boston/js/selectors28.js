function change_size(element, size){
	var current = parseInt(element.css('font-size'));
	var new_size=0;
	
	if (size == 'smaller'){
		new_size=current-1;
	}
	else if (size == 'bigger'){
		new_size=current+2;
	}
	element.css('font-size',new_size+"px");
}

$('#smaller').click(function(){
	change_size ($('p'),'smaller');
});

$('#bigger').click(function(){
	change_size ($('p'),'bigger');
});






