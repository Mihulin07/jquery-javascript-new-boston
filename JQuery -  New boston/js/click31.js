$('#button_hide').click(function() {
	if ($('p').is(":visible")){
		$('p').hide();
		$('#button_hide').attr('value','Show');
	}
	else{
		$('p').show();
		$('#button_hide').attr('value','Hide');
	}
});